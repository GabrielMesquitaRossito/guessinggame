﻿using GuessinAnimaGameWPF.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace GuessinAnimaGameWPF.Tests
{
    [TestClass()]
    public class GuessingTreeTests
    {
        [TestMethod()]
        public void CreationTree()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            Assert.AreEqual(game.CurrentQuestion, "flies");
        }

        [TestMethod()]
        public void YesAction()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();
            game.YesAnswer();
            game.YesAnswer();
            game.YesAnswer();
            game.YesAnswer();
            Assert.AreEqual(game.MakeAGuess().AnimalName, "bird");
        }

        [TestMethod()]
        public void NoAction()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            game.NoAnswer();
            game.NoAnswer();
            game.NoAnswer();
            game.NoAnswer();
            Assert.AreEqual(game.MakeAGuess().AnimalName, "monkey");
        }

        [TestMethod()]
        public void YesAndThenAddNew()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            game.YesAnswer();
            game.YesAnswer();
            game.AddNewGuess("sptis fire", "dragon", game.MakeAGuess());
            game.StartGame();
            game.YesAnswer();
            Assert.AreEqual(game.CurrentQuestion, "sptis fire");
        }

        [TestMethod()]
        public void YesAndThenAddNewAndYesAgain()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            game.YesAnswer();
            game.YesAnswer();
            game.AddNewGuess("sptis fire", "dragon", game.MakeAGuess());
            game.StartGame();
            game.YesAnswer();
            game.YesAnswer();
            Assert.AreEqual(game.MakeAGuess().AnimalName, "dragon");
        }

        [TestMethod()]
        public void YesAndThenAddNewAndThenThinkBird()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            game.YesAnswer();
            game.YesAnswer();
            game.AddNewGuess("sptis fire", "dragon", game.MakeAGuess());
            game.StartGame();
            game.YesAnswer();
            game.NoAnswer();
            Assert.AreEqual(game.MakeAGuess().AnimalName, "bird");
        }

        [TestMethod()]
        public void YesAndThenAddNewAndThenDog()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            game.YesAnswer();
            game.YesAnswer();
            game.AddNewGuess("sptis fire", "dragon", game.MakeAGuess());
            game.StartGame();
            game.NoAnswer();
            game.NoAnswer();
            game.AddNewGuess("bark", "dogo", game.MakeAGuess());
            game.StartGame();
            game.NoAnswer();
            game.YesAnswer();
            Assert.AreEqual(game.MakeAGuess().AnimalName, "dogo");
        }

        [TestMethod()]
        public void YesAndThenAddDragonAddDogoAndThenThinkSquirrel()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            game.YesAnswer();
            game.YesAnswer();
            game.AddNewGuess("sptis fire", "dragon", game.MakeAGuess());
            game.StartGame();
            game.NoAnswer();
            game.NoAnswer();
            game.AddNewGuess("bark", "dogo", game.MakeAGuess());
            game.StartGame();
            game.NoAnswer();
            game.NoAnswer();
            game.NoAnswer();
            game.AddNewGuess("wins thanos in a fight", "squirrel", game.MakeAGuess());
            game.StartGame();
            game.NoAnswer(); // flies?
            game.NoAnswer(); // bark?
            game.YesAnswer(); // wins thanos in a fight?
            Assert.AreEqual(game.MakeAGuess().AnimalName, "squirrel");
        }

        [TestMethod()]
        public void GoBonksWithTheTree()
        {
            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            Game game = new Game(new TreeNode("flies"), startKnowledge);
            game.StartGame();

            game.YesAnswer(); // flies?
            game.YesAnswer(); // null
            game.AddNewGuess("sptis fire", "dragon", game.MakeAGuess());
            game.StartGame();
            game.YesAnswer(); // flies?
            game.NoAnswer(); // spits fire?
            game.AddNewGuess("HUE mode", "https://media.giphy.com/media/g5FB33d3GVUkg/giphy.gif", game.MakeAGuess());
            game.StartGame();
            game.YesAnswer(); // flies?
            game.NoAnswer(); // spits fire?
            game.YesAnswer(); // HUE mode
            Assert.AreEqual(game.MakeAGuess().AnimalName, "https://media.giphy.com/media/g5FB33d3GVUkg/giphy.gif");
        }
    }
}