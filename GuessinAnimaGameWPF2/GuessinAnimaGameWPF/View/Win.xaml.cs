﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace GuessinAnimaGameWPF.View
{
    /// <summary>
    /// Interaction logic for Win.xaml
    /// </summary>
    public partial class Win : Page
    {
        private Action _confirmAction;

        public Win(Action confirmAction)
        {
            InitializeComponent();
            _confirmAction = confirmAction;
        }

        private void BtnConfirm(object sender, RoutedEventArgs e)
        {
            _confirmAction();
        }
    }
}