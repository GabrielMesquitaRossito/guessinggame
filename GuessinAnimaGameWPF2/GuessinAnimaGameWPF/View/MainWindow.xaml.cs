﻿using GuessinAnimaGameWPF.Model;
using System.Collections.Generic;
using System.Windows;

namespace GuessinAnimaGameWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Game game;

        public MainWindow()
        {
            InitializeComponent();

            this.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            List<Guess> startKnowledge = new List<Guess>()
            {
                new Guess("bird", "flies"),
                new Guess("monkey", "flies", false)
            };
            // TODO: create a tree manager to faster design testes
            game = new Game(new TreeNode("flies"), startKnowledge);

            StartGame();
        }

        private void StartGame()
        {
            game.StartGame();
            Main.Content = new Question(game.CurrentQuestion, BtnQuestionYesClicked, BtnQuestionNoClicked);
        }

        private void BtnQuestionYesClicked()
        {
            if (game.YesAnswer())
                Main.Content = new Question(game.CurrentQuestion, BtnQuestionYesClicked, BtnQuestionNoClicked);
            else
                Main.Content = new Answer(game.MakeAGuess().AnimalName, BtnAwnserYesClicked, BtnAwnserNoClicked);
        }

        private void BtnQuestionNoClicked()
        {
            if (game.NoAnswer())
                Main.Content = new Question(game.CurrentQuestion, BtnQuestionYesClicked, BtnQuestionNoClicked);
            else
            {
                Guess guess = game.MakeAGuess();
                Main.Content = new Answer(guess?.AnimalName, BtnAwnserYesClicked, BtnAwnserNoClicked);
            }
        }

        private void BtnAwnserYesClicked()
        {
            Main.Content = new View.Win(StartGame);
        }

        private void BtnAwnserNoClicked()
        {
            WrongAwnser();
        }

        private void WrongAwnser()
        {
            Main.Content = new View.GetInput(game.MakeAGuess().AnimalName, NewNode);
        }

        private void DontHaveMoreSuggestions(string somesuggestion)
        {
            Main.Content = new View.GetInput(somesuggestion, NewNode);
        }

        private void NewNode(string animal, string question)
        {
            game.AddNewGuess(question, animal, game.MakeAGuess());
            StartGame();
        }
    }
}