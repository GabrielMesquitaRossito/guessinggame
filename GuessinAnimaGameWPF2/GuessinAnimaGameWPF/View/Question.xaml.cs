﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace GuessinAnimaGameWPF
{
    /// <summary>
    /// Interaction logic for Question.xaml
    /// </summary>
    public partial class Question : Page
    {
        private System.Action _yesAction, _noAction;

        public Question(string text, Action yesAction, Action noAction)
        {
            InitializeComponent();
            PopupText.Content = string.Format("Does your animal {0}?", text);
            _yesAction = yesAction;
            _noAction = noAction;
        }

        private void BtnNoClick(object sender, RoutedEventArgs e)
        {
            _noAction.Invoke();
        }

        private void BtnYesClick(object sender, RoutedEventArgs e)
        {
            _yesAction.Invoke();
        }
    }
}