﻿using System;
using System.Windows.Controls;

namespace GuessinAnimaGameWPF.View
{
    /// <summary>
    /// Interaction logic for GetInput.xaml
    /// </summary>
    public partial class GetInput : Page
    {
        private string _correctAnimal, _wrongAnimal;
        private Action _confirmBtnAction;
        private Action<string, string> _callback;

        /// <summary>
        /// Window for getting new animal and question
        /// </summary>
        /// <param name="wrongAnimal">name of the wrong guess</param>
        /// <param name="callback">popup return with Animal name and new Question to make respectively</param>
        public GetInput(string wrongAnimal, Action<string, string> callback)
        {
            InitializeComponent();

            _callback = callback;
            _wrongAnimal = wrongAnimal;
            PopupText.Content = "What was the animal that you thought about?";
            message.Content = "";
            _confirmBtnAction = GetQuestion;
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _confirmBtnAction?.Invoke();
        }

        private void GetQuestion()
        {
            _correctAnimal = TextBox.Text;
            TextBox.Text = string.Empty;
            PopupText.Content = string.Format("A {0} _______ but a {1} does not.", _correctAnimal, _wrongAnimal);
            message.Content = "(Fill it with an animal trait, like 'lives in water')";
            _confirmBtnAction = CreateNewGuess;
        }

        private void CreateNewGuess()
        {
            _callback(_correctAnimal, TextBox.Text);
            _confirmBtnAction = null;
        }
    }
}