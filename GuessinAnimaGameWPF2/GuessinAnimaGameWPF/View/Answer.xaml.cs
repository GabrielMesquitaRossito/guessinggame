﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace GuessinAnimaGameWPF
{
    /// <summary>
    /// Interaction logic for Answer.xaml
    /// </summary>
    public partial class Answer : Page
    {
        private System.Action _yesAction, _noAction;

        public Answer(string text, Action yesAction, Action noAction)
        {
            InitializeComponent();

            PopupText.Content = string.Format("Is your animal a {0}?", text);
            _yesAction = yesAction;
            _noAction = noAction;
        }

        private void BtnYesClick(object sender, RoutedEventArgs e)
        {
            _yesAction.Invoke();
        }

        private void BtnNoClick(object sender, RoutedEventArgs e)
        {
            _noAction.Invoke();
        }
    }
}