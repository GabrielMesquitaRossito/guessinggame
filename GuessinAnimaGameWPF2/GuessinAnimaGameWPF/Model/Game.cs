﻿using System.Collections.Generic;

namespace GuessinAnimaGameWPF.Model
{
    public class Game
    {
        public Game(TreeNode starterNode, List<Guess> starterKnowledge)
        {
            StartTree = starterNode;
            DecisionsTaken = new Dictionary<string, bool>();
            History = new List<TreeNode>();
            Knowlege = new Knowlegement(starterKnowledge);
        }

        private TreeNode CurrentNode;
        private TreeNode StartTree;
        private Knowlegement Knowlege;
        private Dictionary<string, bool> DecisionsTaken;
        private List<TreeNode> History;
        private bool LastAnswerYes;

        public string CurrentQuestion
        {
            get
            {
                if (CurrentNode != null)
                    return CurrentNode.Question;
                return "";
            }
        }

        public string LastQuestion
        {
            get
            {
                if (History != null && History.Count > 0)
                    return History[History.Count - 1]?.Question;
                return "";
            }
        }

        public Guess MakeAGuess()
        {
            return Knowlege.MakeAGuess();
        }

        public void StartGame()
        {
            CurrentNode = StartTree;
            DecisionsTaken = new Dictionary<string, bool>();
            History = new List<TreeNode>();
            Knowlege.StartPoints();
        }

        public bool NoAnswer()
        {
            if (CurrentNode != null)
            {
                RegisterDecisionAndBalanceGuesses(CurrentNode.Question, false);
                CurrentNode = CurrentNode.No;
                LastAnswerYes = false;
            }
            return CurrentNode != null;
        }

        public bool YesAnswer()
        {
            if (CurrentNode != null)
            {
                RegisterDecisionAndBalanceGuesses(CurrentNode.Question, true);
                CurrentNode = CurrentNode.Yes;
                LastAnswerYes = true;
            }
            return CurrentNode != null;
        }

        public void AddNewGuess(string question, string answer, Guess wrongGuess)
        {
            DecisionsTaken.Add(question, true); // Adiciona um novo imput de decisão tomada como correta para ser utilizada no histórico da nova sugestão
            wrongGuess.Avaliation.Add(question, false); // Adiciona a nova sugestão de negação para a sugestão errada
            TreeNode newNode = new TreeNode(question); // Cria a nova pergunta
            if (LastAnswerYes)
                History[History.Count - 1].Yes = newNode; // adiciona a ávore
            else
                History[History.Count - 1].No = newNode; // adiciona a ávore
            Knowlege.AddGuess(new Guess(answer, DecisionsTaken));
        }

        private void RegisterDecisionAndBalanceGuesses(string question, bool value)
        {
            History.Add(CurrentNode);
            DecisionsTaken.Add(question, value);
            Knowlege.BalancePoints(question, value);
        }
    }
}