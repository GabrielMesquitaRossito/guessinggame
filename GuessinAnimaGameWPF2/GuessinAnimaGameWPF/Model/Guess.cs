﻿using System.Collections.Generic;

namespace GuessinAnimaGameWPF.Model
{
    public class Guess
    {
        public Guess(string animalName, string question, bool valid = true)
        {
            AnimalName = animalName;
            Avaliation = new Dictionary<string, bool>();
            Avaliation.Add(question, valid);
        }

        public Guess(string text, Dictionary<string, bool> avaliation)
        {
            AnimalName = text;
            Avaliation = avaliation;
        }

        public string AnimalName;
        public Dictionary<string, bool> Avaliation;

        public int Points { get; private set; }

        public void CalculatePoints(string question, bool awnser)
        {
            if (Avaliation.ContainsKey(question) && Avaliation[question] == awnser)
                Points++;
        }

        public void Restart()
        {
            Points = 0;
        }
    }
}