﻿using System.Collections.Generic;
using System.Linq;

namespace GuessinAnimaGameWPF.Model
{
    internal class Knowlegement
    {
        public Knowlegement(List<Guess> starter)
        {
            _knownGuess = starter;
        }

        private List<Guess> _knownGuess;

        public Guess MakeAGuess()
        {
            return _knownGuess.OrderByDescending(x => x.Points).FirstOrDefault(); // TODO: fazer iteração de alteração de index para cada calculo de pontuação
        }

        public void BalancePoints(string question, bool value)
        {
            for (int i = 0; i < _knownGuess.Count; i++)
                _knownGuess[i].CalculatePoints(question, value);
        }

        public void StartPoints()
        {
            for (int i = 0; i < _knownGuess.Count; i++)
                _knownGuess[i].Restart();
        }

        public void AddGuess(Guess guess)
        {
            _knownGuess.Add(guess);
        }
    }
}