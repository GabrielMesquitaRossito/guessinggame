﻿namespace GuessinAnimaGameWPF.Model
{
    public class TreeNode
    {
        public TreeNode(string text)
        {
            Question = text;
        }

        public string Question;
        public TreeNode Yes;
        public TreeNode No;
    }
}